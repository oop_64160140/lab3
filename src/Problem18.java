import java.util.Scanner;

public class Problem18 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int type , number;
        do {
            System.out.print("Please select star type[1-4,5 is Exit]: ");
            type = sc.nextInt();

            if(type==1){
                System.out.print("Please input number: ");
                number = sc.nextInt();
                for(int i=1; i<=number; i++){
                    for (int j=1; j<=i; j++){
                        System.out.print("*");
                    }
                    System.out.println();
                }
            }else if(type==2){
                System.out.print("Please input number: ");
                number = sc.nextInt();
                for(int i=number; i>0; i--){
                    for(int j=0; j<i; j++){
                        System.out.print("*");
                    }
                    System.out.println();
                }
            }else if(type==3){
                System.out.print("Please input number: ");
                number = sc.nextInt();
                for(int i=0; i<number; i++){
                    for(int j=0; j<i; j++){
                        System.out.print(" ");
                    }
                    for(int j=0; j<number-i; j++){
                            System.out.print("*");
                    }
                    System.out.println();
                }
            }else if(type==4){
                System.out.print("Please input number: ");
                number = sc.nextInt();
                for(int i=4; i>=0; i--){
                    for(int j=0; j<number; j++){
                        if(j>=i){
                            System.out.print("*");  
                        } else {
                            System.out.print(" ");
                        }
                    }
                    System.out.println();
                }
            }else if(type==5){
                System.out.println("Bye bye!!!");
                break;    //Exit
            }else if(type==6){
                System.out.println("Error: Please input number between 1-5");
                continue;
            } 
        }while(type!=5);  
    }
}
